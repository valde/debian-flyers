# Logo 30 years Debian french

All artwork in this repository has been produced collaboratively and is
licensed in a way that allows such reproduction.

## Author:

* Laure Sivadier

## License:

[Creative Commons Attribution Share Alike 4.0](https://salsa.debian.org/debconf-team/public/data/dc19/blob/master/LICENSE.txt)
